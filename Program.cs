﻿using System;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            string userName = "";

            Console.WriteLine("*********************************");
            Console.WriteLine("****    Welcome to my app    ****");
            Console.WriteLine("*********************************\n\n");

            Console.WriteLine("*********************************");
            Console.WriteLine("What is your name?");
            userName = Console.ReadLine();
            Console.WriteLine($"Your name is: {userName}");

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
